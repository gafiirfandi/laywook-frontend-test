# Laywook - Frontend

## Pipeline Status

[![pipeline status](https://gitlab.com/gafiirfandi/laywook-frontend-test/badges/master/pipeline.svg)](https://gitlab.com/gafiirfandi/laywook-frontend-test/-/commits/master)

## Coverage Status

[![coverage report](https://gitlab.com/gafiirfandi/laywook-frontend-test/badges/master/coverage.svg)](https://gitlab.com/gafiirfandi/laywook-frontend-test/-/commits/master)

## Documentation

TBA